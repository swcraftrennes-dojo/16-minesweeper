# 16 - Minesweeper

## Subject

Minesweeper is a popular game where the user has to find the mines using numeric hints that indicate how many mines are 
directly adjacent (**horizontally**, **vertically**, **diagonally**) to a square.

In this exercise you have to create some code that counts the number of mines adjacent to a square and transforms boards
like this (where `*` indicates a mine):

```
+-----+
| * * |
|  *  |
|  *  |
|     |
+-----+
```

into :
```
+-----+
|1*3*1|
|13*31|
| 2*2 |
| 111 |
+-----+
```

Example input : A List of Strings.
```
[
        " *  * ",
        "  *   ",
        "    * ",
        "   * *",
        " *  * ",
        "      "
]
```

Expected output : A List of Strings.
```
[
        "1*22*1",
        "12*322",
        " 123*2",
        "112*4*",
        "1*22*2",
        "111111"
]
```

**Leave a space, in case there are no nearby mines.**

### Hints

- You can use 2D Arrays (i.e. 2 dimensional lists in dart).
- You can also utilize Maps.
- You can create your own 1 or more custom data types (class).

## Installation

Go through the [setup instructions](https://dart.dev/get-dart) for Dart to install the necessary
dependencies.

## How do I launch the tests ?

Execute the tests with:
```
$ pub run test
```
